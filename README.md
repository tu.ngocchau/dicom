## Getting started
# dicom

The purpose of this project is to provide a DICOM service that combines the use of PostgreSQL and Minio storage. DICOM (Digital Imaging and Communications in Medicine) is a standard for transmitting, storing, and sharing medical images and related information.

## Getting started
To get started with this project, follow the steps below:
1. Install Rancher-desktop (with moby/dockerd, not nerdctl)
2. Setup Tilt 
3. Clone this repository: `git clone https://github.com/your-username/dicom.git`
4. `tilt up`

Please refer to the project documentation for more detailed information on how to use the DICOM service and its features.

